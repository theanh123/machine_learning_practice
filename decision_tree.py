from sklearn import tree
from sklearn.datasets import load_iris
import numpy as np
import random


def f1():
    X = [[0, 0], [2, 2]]
    y = [0.5, 2.5]
    clf = tree.DecisionTreeRegressor()
    clf = clf.fit(X, y)
    output = clf.predict([[1, 1]])
    print(output)


def f2():
    iris = load_iris()
    clf = tree.DecisionTreeClassifier()
    clf = clf.fit(iris.data, iris.target)
    # import graphviz
    # dot_data = tree.export_graphviz(clf, out_file=None)
    # dot_data = tree.export_graphviz(clf, out_file=None,
    #                                 feature_names=iris.feature_names,
    #                                 class_names=iris.target_names,
    #                                 filled=True, rounded=True,
    #                                 special_characters=True)
    # graph = graphviz.Source(dot_data)
    # graph.render("iris")
    # print(graph)
    predicts = clf.predict(iris.data)
    y = iris.target
    c = 0
    for i in range(len(iris.data)):
        if predicts[i] == y[i]:
            c += 1
    print(c)


def decision_tree_classifier(trainx, trainy, testx, testy, c):
    clf = tree.DecisionTreeClassifier()
    clf = clf.fit(trainx, trainy)
    import graphviz
    dot_data = tree.export_graphviz(clf, out_file=None)
    graph = graphviz.Source(dot_data)
    graph.render("iris" + str(c))
    # print(graph)
    predicts = clf.predict(testx)
    y = testy
    c = 0
    for i in range(len(predicts)):
        if predicts[i] == y[i]:
            c += 1
    return(c/len(testy))


def test_DT():
    iris = load_iris()
    data = iris.data
    target = iris.target
    l_index = list(range(len(data)))
    random.shuffle(l_index)
    data = data[l_index]
    target = target[l_index]
    print(data.shape, target.shape)
    pieces_len = len(data)/3
    precision = 0
    for j in range(100):
        for i in range(3):
            si = int(i * pieces_len)
            ei = int((i + 1) * pieces_len)
            testx = data[si:ei]
            testy = target[si:ei]
            trainx = np.concatenate((data[:si], data[ei:]), axis=0)
            trainy = np.concatenate((target[:si], target[ei:]), axis=0)
            precision += decision_tree_classifier(trainx=trainx, trainy=trainy, testx=testx, testy=testy, c=i)
    print('test_DT', precision/300)

if __name__ == '__main__':
    test_DT()
