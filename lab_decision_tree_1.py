from sklearn import tree

X = [[0, 0], [2, 2]]
y = [0, 1]
clf = tree.DecisionTreeRegressor()
clf = clf.fit(X, y)
output = clf.predict([[1, 1]])
print(output)
