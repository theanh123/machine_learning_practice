from sklearn.datasets import load_iris
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import ExtraTreesClassifier
import random
import numpy as np
from sklearn import tree


def decision_tree_classifier(trainx, trainy, testx, testy):
    clf = tree.DecisionTreeClassifier()
    clf.fit(trainx, trainy)
    predicts = clf.predict(testx)
    y = testy
    c = 0
    for i in range(len(predicts)):
        if predicts[i] == y[i]:
            c += 1
    return c/len(testy)


def random_forest_classifier(trainx, trainy, testx, testy):
    clf = RandomForestClassifier(n_estimators=10, max_depth=None,
                                 min_samples_split=2, random_state=0)
    clf.fit(trainx, trainy)
    predicts = clf.predict(testx)
    y = testy
    c = 0
    for i in range(len(predicts)):
        if predicts[i] == y[i]:
            c += 1
    return c/len(testy)


def extra_trees_classifier(trainx, trainy, testx, testy):
    clf = ExtraTreesClassifier(n_estimators=10, max_depth=None,
                               min_samples_split=2, random_state=0)
    clf.fit(trainx, trainy)
    predicts = clf.predict(testx)
    y = testy
    c = 0
    for i in range(len(predicts)):
        if predicts[i] == y[i]:
            c += 1
    return c/len(testy)


def test_rf_dt_et():
    iris = load_iris()
    data = iris.data
    target = iris.target
    l_index = list(range(len(data)))
    random.shuffle(l_index)
    data = data[l_index]
    target = target[l_index]
    print(data.shape, target.shape)
    pieces_len = len(data)/3
    precision_rf = 0
    precision_dt = 0
    precision_et = 0
    for j in range(100):
        for i in range(3):
            si = int(i * pieces_len)
            ei = int((i + 1) * pieces_len)
            testx = data[si:ei]
            testy = target[si:ei]
            trainx = np.concatenate((data[:si], data[ei:]), axis=0)
            trainy = np.concatenate((target[:si], target[ei:]), axis=0)
            precision_rf += random_forest_classifier(trainx=trainx, trainy=trainy, testx=testx, testy=testy)
            precision_dt += decision_tree_classifier(trainx=trainx, trainy=trainy, testx=testx, testy=testy)
            precision_et += extra_trees_classifier(trainx=trainx, trainy=trainy, testx=testx, testy=testy)
    print('test_RF', precision_rf/300)
    print('test_DT', precision_dt / 300)
    print('test_ET', precision_et / 300)


if __name__ == '__main__':
    test_rf_dt_et()